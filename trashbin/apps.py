# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TrashbinConfig(AppConfig):
    name = 'trashbin'
