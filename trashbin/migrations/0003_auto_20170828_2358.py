# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-28 20:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trashbin', '0002_remove_trashbin_index'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trashbin',
            name='max_count_elem',
            field=models.IntegerField(verbose_name='Trashbin max count elem'),
        ),
        migrations.AlterField(
            model_name='trashbin',
            name='trashbin_path',
            field=models.CharField(max_length=265, verbose_name='Trashbin path'),
        ),
        migrations.AlterField(
            model_name='trashbin',
            name='trashbin_size',
            field=models.IntegerField(verbose_name='Trashbin size'),
        ),
    ]
