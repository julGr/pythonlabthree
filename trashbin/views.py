# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect

from .forms import TrashbinForm
from .models import Trashbin


def trashbin_list(request):
    trashbins = Trashbin.objects.all()

    if request.method == 'POST':
        if 'remove_btn' in request.POST:
            x = Trashbin.objects.get(id=request.POST['remove_btn'])
            x.delete()
    return render(request, 'trashbin/trashbin_list.html', {'trashbins': trashbins})


def delete_trash(request, pk):
    trash_model = get_object_or_404(Trashbin, id=pk)
    trash_model.delete()
    trash_model.remove_trashbin()
    return HttpResponseRedirect(reverse('trashbin_list'))


def trashbin_detail(request, pk):
    trashbin = get_object_or_404(Trashbin, pk=pk)
    if trashbin.is_empty():
        content = None
    else:
        content = trashbin.get_trash_content()[1]
    return render(request, 'trashbin/trashbin_detail.html', {'content': content, 'trashbin': trashbin})


def trashbin_new(request):
    if request.method == "POST":
        form = TrashbinForm(request.POST)
        if form.is_valid():
            trashbin = form.save()
            trashbin.save()
            Trashbin.objects.latest('id').create()
            return redirect('trashbin_list')
    else:
        form = TrashbinForm()
    return render(request, 'trashbin/trashbin_edit.html', {'form': form})

