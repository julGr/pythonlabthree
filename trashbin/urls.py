#coding: utf-8

from django.conf.urls import url

from . import views


urlpatterns = [
	url(r'^$', views.trashbin_list, name='trashbin_list'),
	url(r'^trashbin/(?P<pk>[0-9]+)/$', views.trashbin_detail, name='trashbin_detail'),
	url(r'^trashbin/new/$', views.trashbin_new, name='trashbin_new'),
	url(r'^trashbin/(?P<pk>[0-9]+)/delete/$', views.delete_trash, name='delete_trash'),
]
