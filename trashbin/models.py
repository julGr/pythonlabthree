# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import shutil
import os

from django.db import models
from django.core.validators import MinValueValidator
from rm_package.trashbin import Trashbin as trbin


class Trashbin(models.Model):
    trashbin_path = models.CharField('Trashbin path', max_length=265)
    trashbin_size = models.PositiveIntegerField('Trashbin size', validators=[MinValueValidator(1)])
    max_count_elem = models.PositiveIntegerField('Trashbin max count elem', validators=[MinValueValidator(1)])

    def __str__(self):
        return self.trashbin_path

    def create(self):
        trbin(trashbin_path=self.trashbin_path,
              trashbin_max_size=self.trashbin_size,
              max_count_elem=self.max_count_elem)

    def get_size(self):
        trashbin = trbin(trashbin_path=self.trashbin_path,
                         trashbin_max_size=self.trashbin_size,
                         max_count_elem=self.max_count_elem)
        return trashbin.get_size()

    def get_trash_content(self):
        trashbin = trbin(trashbin_path=self.trashbin_path,
                         trashbin_max_size=self.trashbin_size,
                         max_count_elem=self.max_count_elem)
        return trashbin.get_trash_content()

    def is_empty(self):
        trashbin = trbin(trashbin_path=self.trashbin_path,
                         trashbin_max_size=self.trashbin_size,
                         max_count_elem=self.max_count_elem)
        return trashbin.is_empty()

    def remove_trashbin(self):
        shutil.rmtree(os.path.expanduser(self.trashbin_path))
