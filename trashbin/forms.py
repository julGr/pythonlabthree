from django import forms
from .models import Trashbin
from os.path import exists, expanduser


class TrashbinForm(forms.ModelForm):
    class Meta:
        model = Trashbin
        fields = ('trashbin_path', 'trashbin_size', 'max_count_elem',)

    def clean_trashbin_path(self):
        data = expanduser(self.cleaned_data['trashbin_path'])
        if exists(data):
            raise forms.ValidationError('Already exists folder with this name')
        return data