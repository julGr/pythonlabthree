# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
import os
import shutil
from django.test import TestCase

from .models import Trashbin
from .forms import TrashbinForm


def create_test_trashbin():
    return Trashbin.objects.create(trashbin_path='/home/julia/testTrashbin',
                                   trashbin_size=100000,
                                   max_count_elem=1000)


class TrashbinTestCase(TestCase):
    def test_empty_trashbin_list(self):
        response = self.client.get(reverse('trashbin_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No trashbins")
        self.assertQuerysetEqual(response.context['trashbins'], [])

    def test_trashbin_list(self):
        create_test_trashbin()
        response = self.client.get(reverse('trashbin_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Trashbin №")
        self.assertQuerysetEqual(response.context['trashbins'],
                                 ['<Trashbin: /home/julia/testTrashbin>'])

    def test_trashbin_creation(self):
        response = self.client.get(reverse('trashbin_new'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Trashbin creation")

    def test_trashbin_emptiness(self):
        test_trashbin = create_test_trashbin()
        url = reverse('trashbin_detail', args=(test_trashbin.id, ))
        response = self.client.get(url)
        self.assertContains(response, "The trashbin is empty")

    def tearDown(self):
        if os.path.exists('/home/julia/testTrashbin'):
            shutil.rmtree('/home/julia/testTrashbin')


class TrashbinFormTestCase(TestCase):
    def setUp(self):
        self.data_for_form = {'trashbin_path': '/home/julia/testTrashbin',
                              'trashbin_size': 100000,
                              'max_count_elem': 1000}

    def test_form_validity(self):
        form = TrashbinForm(self.data_for_form)
        self.assertTrue(form.is_valid())

    def test_form_with_negative_num(self):
        self.data_for_form['trashbin_size'] = -1
        form = TrashbinForm(self.data_for_form)
        self.assertFalse(form.is_valid())

    def test_form_with_missing_path(self):
        self.data_for_form['trashbin_path'] = ''
        form = TrashbinForm(self.data_for_form)
        self.assertFalse(form.is_valid())

    def test_form_wuth_existing_path(self):
        os.mkdir(self.data_for_form['trashbin_path'])
        form = TrashbinForm(self.data_for_form)
        self.assertFalse(form.is_valid())

    def tearDown(self):
        if os.path.exists(self.data_for_form['trashbin_path']):
            shutil.rmtree(self.data_for_form['trashbin_path'])


