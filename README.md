
#webRM
-------------

Modified program to delete files and folders with the addition of a web interface and parallel execution.

###List of basic operations:

- View known trashbins.
- Create, manage, display status and removal of trashbins.
- View tasks.
- Create, manage and display the status of tasks.
- Parallelizing by directories

###What you need
To install this software, you need to have installed **django**  and be in your virtual environment.

> To install django type:
```
pip install django
```

>To install virtual environment type:
```
pip install vertualenv
```

>Two base commands are:
```
virtualenv (name)
sourse ./(name)/bin/activate
```
>The first command creates isolate environment in folder '(name)'. The second command activates the environment.


##Start

```
source ./env/bin/activate
(~/.../webrm$) python manage.py runserver
```

##Instruction

The start page contains list of created trashbins. Your can get trashbin's content by click on it's name.
Click on 'Create trashbin' in the upper left corner of the screen for creating new trashbin.
You will see quite simple form that contains fields 'Trashbin path', 'Trashbin size', 'Trashbin max count elem'.
Click on 'Tasks'. You will see form for creating a new task and section 'Tasks history'.
You can create a regular expression for convenient removal of multiple files.
