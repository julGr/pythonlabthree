# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from trashbin.models import Trashbin

from rm_package.restore import restore
from rm_package.remove import remove, regex_remove


class Task(models.Model):
    RM = 'rm'
    RS = 'rs'
    action = (
        (RM, 'remove'),
        (RS, 'restore'),
    )

    action = models.CharField(choices=action, default=RM, max_length=2)
    file_name = models.CharField("Enter path to file", max_length=265)
    regex = models.CharField("Text regex", max_length=265, blank=True, help_text="Option for removing")
    if Trashbin.objects.all():
        trashbin_id = Trashbin.objects.first().id
    else:
        trashbin_id = None

    trashbin = models.ForeignKey(Trashbin, default="", blank=False)

    def select_action(self):
        if self.action == 'rm':
            if self.regex == '':
                return remove(self.file_name, self.trashbin.trashbin_path)
            else:
                return regex_remove(self.file_name, self.trashbin.trashbin_path, self.regex)
        if self.action == 'rs':
            return restore(self.file_name, self.trashbin.trashbin_path)
