# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse

from tasks.forms import TaskForm
from tasks.models import Task


def task_list(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            Task.objects.latest('id').select_action()
            return redirect('tasks_history')
    else:
        form = TaskForm()
    return render(request, 'tasks/task_list.html', {'form': form})


def tasks_history(request):
    tasks = Task.objects.all()
    return render(request, 'tasks/tasks_history.html', {'tasks': tasks})


def clean_history(request):
    for task in Task.objects.all():
        task.delete()
    return HttpResponseRedirect(reverse('trashbin_list'))


