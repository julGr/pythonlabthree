# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.test import TestCase

from tasks.models import Task
from tasks.forms import TaskForm
from trashbin.models import Trashbin


def create_test_trashbin():
    return Trashbin.objects.create(trashbin_path='/home/julia/testTrashbin',
                                   trashbin_size=100000,
                                   max_count_elem=1000)


def create_test_task():
    with open('/home/julia/test_file', 'w') as f:
        pass
    return Task.objects.create(action='rm',
                               file_name='/home/julia/test_file',
                               regex='',
                               trashbin=create_test_trashbin())


class TaskTestCase(TestCase):
    def test_task_history_emptiness(self):
        response = self.client.get(reverse('tasks_history'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No tasks')
        self.assertQuerysetEqual(response.context['tasks'], [])

    def test_task_history(self):
        create_test_task()
        response = self.client.get(reverse('tasks_history'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Task №')
        self.assertQuerysetEqual(response.context['tasks'],
                                 ['<Task: Task object>'])

    def test_task_creation(self):
        response = self.client.get(reverse('task_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Task creation')
"""
    def test_rm(self):
        test_task = create_test_task()
        test_task.select_action()
        self.assertFalse(os.path.exists('/home/julia/test_file'))
"""
class TaskFormTestCase(TestCase):
    def setUp(self):
        create_test_trashbin()
        with open('/home/julia/test_file', 'w') as f:
            pass
        self.data_for_form = {'action': 'rm',
                              'file_name': '/home/julia/test_file',
                              'regex': "",
                              'trashbin': Trashbin.objects.get(trashbin_path='/home/julia/testTrashbin')}

    def test_form_validity(self):
        self.data_for_form['trashbin'] = create_test_trashbin()
        form = TaskForm(self.data_for_form)
        print form.errors
        self.assertTrue(form.is_valid())
