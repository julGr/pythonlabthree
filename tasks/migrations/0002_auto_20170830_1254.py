# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-30 09:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(choices=[('rm', 'remove'), ('rs', 'restore')], default='rm', max_length=2),
        ),
        migrations.AlterField(
            model_name='task',
            name='regex',
            field=models.CharField(blank=True, max_length=265, verbose_name='Text regex'),
        ),
    ]
