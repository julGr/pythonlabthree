#coding: utf-8

from django.conf.urls import url

from tasks import views

urlpatterns = [
	url(r'^tasks/$', views.task_list, name='task_list'),
	url(r'^tasks_history/$', views.tasks_history, name='tasks_history'),
	url(r'^task/clean/$', views.clean_history, name='clean_history')
]
