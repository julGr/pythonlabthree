import re
import os
from os.path import expanduser, exists, join
from django import forms

import json

from tasks.models import Task
from tasks.models import Trashbin


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('action', 'file_name', 'regex', 'trashbin',)

    def clean(self):
        if self.cleaned_data.get('trashbin'):
            f_action = self.cleaned_data.get('action')
            f_file_name = expanduser(self.cleaned_data.get('file_name'))
            f_regex = self.cleaned_data.get('regex')
            f_trashbin = self.cleaned_data.get('trashbin')

            try:
                with open(join(f_trashbin.trashbin_path, '.info'), 'r') as f:
                    file_list = json.loads(f.read())
            except:
                file_list = {}

            if f_action == 'rm' and not exists(f_file_name):
                raise forms.ValidationError("There is no file with that name")
            if f_action == 'rm' and Trashbin.objects.filter(trashbin_path=f_file_name).exists():
                raise forms.ValidationError("This path belongs to the trashbin."
                                            "You can't remove trashbin")
            if f_action == 'rm' and f_regex != "":
                counter = 0
                for elem in os.listdir(f_file_name):
                    if re.findall(f_regex, elem):
                        counter += 1
                if counter == 0:
                    raise forms.ValidationError("Can't find files by regex")

            if f_action == 'rm' and not os.access(f_file_name, os.X_OK):
                raise forms.ValidationError("Permission denied")

            if f_action == 'rs' and not exists(join(f_trashbin.trashbin_path, f_file_name)):
                raise forms.ValidationError("No such file in the trashbin")
            if f_action == 'rs' and f_regex != "":
                raise forms.ValidationError("Option 'regex' only for removal")
            if f_action == 'rs' and exists(file_list[f_file_name]["name"]):
                raise forms.ValidationError("The previous folder of the file already exists file with that name")

        return self.cleaned_data
